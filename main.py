import urllib3
import json
urllib3.disable_warnings()

POOL = urllib3.PoolManager()
TOKEN = "Your Token"


def request(method, data):
    req = POOL.request("POST", "https://api.telegram.org/bot{}/{}".format(TOKEN, method), data)
    if isinstance(req, urllib3.response.HTTPResponse):
        if req.status == 200:
            return json.loads(req.data.decode())['result']


def on_message(message):
    if isinstance(message, dict):
        if 'text' in message:
            chat_id = message['chat']['id']

            request("sendMessage", {'chat_id': chat_id,
                                    'text': "Hello, World"})


if __name__ == '__main__':
    offset = 0
    while 1:
        updates = request("getUpdates", {'offset': offset + 1, 'timeout': 100})
        if updates:
            for result in updates:
                if 'update_id' in result and result['update_id'] > offset:
                    offset = result['update_id']
                    if 'message' in result:
                        on_message(result['message'])
